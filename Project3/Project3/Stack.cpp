#include "Struct.h"
#include "Stack.h"
#include <iostream>

void create(TNode** top, int d) {
    *top = new TNode;
    (*top)->d = d;
    (*top)->p = NULL;
}
void push(TNode** top, int d) {
    TNode* pv = new TNode;
    pv->d = d;
    pv->p = *top;
    *top = pv;
}

int pop(TNode** top) {
    int temp = (*top)->d;
    TNode* pv = *top;
    *top = (*top)->p;
    delete pv;
    return temp;
}