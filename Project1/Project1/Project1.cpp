﻿// Project1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;
int main()
{
	int  n = 0;
	float a = 1, b = 2, sum = 0.5;
	cout << "Enter the number of steps: ";
	cin >> n;
	for (int i = 0; i < n; i++) {
		sum *= (a + 2) / (b + 2);
	}
	cout << "Sum = " << sum;
}

/*
int main()
{
	double sum = 0, y = 0;
	int i = 1, n = 0;
	cin >> n;
	while (i <= n) {
		y = (1 / (pow((i), 5)));
		sum = sum + y;
		i = i + 1;
	}
	cout << sum;
}

int main() 
{
	float x = -4, y = 0;
	do {
		y = cos(2 * x);
		x += 0.2;
		cout << "y= " << y << endl;
	} while (x <= 4);
}
*/
