﻿// Project2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

int main()
{
    const int n = 5;
    int a[n][n] = { {5,4,3,2,5},{9,8,7,4,6},{9,5,1,4,7},{9,2,5,7,8},{9,5,6,0,1} };
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            cout << a[i][j] << " ";
        cout << endl;
    }
    for (int i = 0; i < n; ++i)
    {
        int min = a[i][0];
        int max = a[i][0];
        for (int j = 1; j < n; ++j)
        {
            if (a[i][j] < min)
                min = a[i][j];
            if (a[i][j] > max)
                max = a[i][j];
        }
        cout <<"The difference between the maximum and minimum values in " <<i + 1<< " row = "<< max - min<< endl;
    }
    cout<< endl;
}


